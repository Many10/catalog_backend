package com.tvshows.catalog.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.api.handler.RestHandler;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.mapper.EpisodeMapper;
import com.tvshows.dto.EpisodeDto;
import com.tvshows.dto.SeasonDto;

@Service
public class EpisodeService {

	private final RestHandler handler;
	private static final Logger logger = LogManager.getLogger(EpisodeService.class);
	private static String _METHOD_NAME;
	
	@Autowired
	public EpisodeService(RestHandler handler) {
		this.handler = handler;
	}
	
	public List<EpisodeDto> getEpisodesBySeason(long idSeason) throws CatalogException {
		try {
			_METHOD_NAME = "getEpisodesBySeason";
			String jsonResponse = handler.executeGetRequest("seasons/" + idSeason + "/episodes");
			JSONArray json = new JSONArray(jsonResponse);
			List<EpisodeDto> episodes = EpisodeMapper.createListEpisodesFromJsonArray(json);
			return episodes;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
	public List<EpisodeDto> getEpisodesByDate(long idShow, String date) throws CatalogException {
		try {
			_METHOD_NAME = "getEpisodesByDate";
			String jsonResponse = handler.executeGetRequest("shows/" + idShow + "/episodesbydate?date=" + date);
			JSONArray json = new JSONArray(jsonResponse);
			List<EpisodeDto> episodes = EpisodeMapper.createListEpisodesFromJsonArray(json);
			return episodes;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
}
