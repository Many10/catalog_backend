package com.tvshows.catalog.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.dao.interfaces.IDaoRole;
import com.tvshows.catalog.entity.Role;
import com.tvshows.catalog.entity.User;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.mapper.RoleMapper;
import com.tvshows.catalog.mapper.UserMapper;
import com.tvshows.dto.RoleDto;

@Service
public class RoleService {

	private final IDaoRole daoRole;
	private static final Logger logger = LogManager.getLogger(RoleService.class);
	private static String _METHOD_NAME;

	@Autowired
	public RoleService(IDaoRole daoRole) {
		this.daoRole = daoRole;
	}

	public RoleDto addRole(RoleDto role) throws CatalogException {
		try {
			_METHOD_NAME = "addRole";
			Role newRole = RoleMapper.createEntityFromDto(role);
			return RoleMapper.createDtoFromEntity(daoRole.save(newRole));
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public List<RoleDto> getAllRole() throws CatalogException {
		try {
			_METHOD_NAME = "getAllRole";
			return RoleMapper.createListDtoFromEntity(daoRole.findAll());
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public RoleDto getRoleById(long id) throws CatalogException {
		try {
			_METHOD_NAME = "getRoleById";
			return RoleMapper.createDtoFromEntity(daoRole.findById(id).orElse(null));
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public int deleteRole(long id) throws CatalogException {
		try {
			_METHOD_NAME = "deleteRole";
			RoleDto deleteRole = this.getRoleById(id);
			daoRole.delete(RoleMapper.createEntityFromDto(deleteRole));
			return 1;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public RoleDto updateRole(long id, RoleDto newRole) throws CatalogException {
		try {
			_METHOD_NAME = "updateRole";
			Role entity = RoleMapper.createEntityFromDto(newRole);
			Role oldRole = daoRole.findById(id).orElse(entity);
			if (oldRole != null) {
				Role newEntity = RoleMapper.createEntityFromDto(newRole);
				newEntity.setId(oldRole.getId());
				return RoleMapper.createDtoFromEntity(daoRole.save(newEntity));
			}
			return null;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

}
