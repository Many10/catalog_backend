package com.tvshows.catalog.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.api.handler.RestHandler;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.mapper.CastMapper;
import com.tvshows.dto.CastDto;

@Service
public class CastService {

	private final RestHandler handler;
	private static final Logger logger = LogManager.getLogger(CastService.class);
	private static String _METHOD_NAME;
	
	@Autowired
	public CastService(RestHandler handler) {
		this.handler = handler;
	}
	
	public List<CastDto> getCastByShow(long idShow) throws CatalogException {
		try {
			_METHOD_NAME = "getCastByShow";
			String jsonResponse = handler.executeGetRequest("shows/" + idShow + "/cast");
			JSONArray json = new JSONArray(jsonResponse);
			List<CastDto> cast = CastMapper.createListCastFromJsonArray(json);
			return cast;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
}
