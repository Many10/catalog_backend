package com.tvshows.catalog.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.api.UserController;
import com.tvshows.catalog.api.handler.RestHandler;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.exceptions.NotFoundException;
import com.tvshows.catalog.mapper.ShowMapper;
import com.tvshows.dto.ShowDto;

@Service
public class ShowService {

	private final RestHandler handler;
	private static final Logger logger = LogManager.getLogger(ShowService.class);
	private static String _METHOD_NAME;
	
	@Autowired
	public ShowService(RestHandler handler) {
		this.handler = handler;
	}
	
	public List<ShowDto> getAllShow() throws CatalogException {
		try {
			_METHOD_NAME = "getAllShow";
			String jsonResponse = handler.executeGetRequest("shows");
			JSONArray json = new JSONArray(jsonResponse);
			List<ShowDto> shows = ShowMapper.createListShowFromJsonArray(json, true);
			if (shows != null && shows.isEmpty()) {
				throw new NotFoundException(_METHOD_NAME, new Exception());				
			}
			return shows;
		} catch (Exception e) {
			CatalogException cex = new CatalogException( _METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
	public ShowDto getShowById(long id) throws CatalogException {
		try {
			_METHOD_NAME = "getShowById";
			String jsonResponse = handler.executeGetRequest("shows/" + id);
			JSONObject json = new JSONObject(jsonResponse);
			ShowDto show = ShowMapper.createShowFromJson(json);
			return show;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
	public List<ShowDto> filterShow(String keyName, String language, String genre, String channel, String scheduleTime) throws CatalogException  {
		try {
			_METHOD_NAME = "filterShow";
			String jsonResponse = keyName.isEmpty() ?  handler.executeGetRequest("shows") : handler.executeGetRequest("search/shows?q=" + keyName);
			JSONArray json = new JSONArray(jsonResponse);
			List<ShowDto> shows = ShowMapper.createListShowFromJsonArray(json, keyName.isEmpty());
			List<ShowDto> resultSet = shows.stream().filter(x -> language.trim().isEmpty() || x.getLanguage().equals(language.trim()))
										  .filter(x -> genre.trim().isEmpty() || x.getGenre().contains(genre.trim()))
										  .filter(x -> channel.trim().isEmpty() || x.getChannel().equals(channel.trim()))
										  .filter(x -> scheduleTime.trim().isEmpty() || x.getScheduleTime().equals(scheduleTime.trim()))
										  .collect(Collectors.toList());
			return resultSet;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
}
