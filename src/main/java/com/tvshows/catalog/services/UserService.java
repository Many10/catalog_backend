package com.tvshows.catalog.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.dao.interfaces.IDaoUser;
import com.tvshows.catalog.entity.User;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.mapper.UserMapper;
import com.tvshows.dto.UserDto;

@Service
public class UserService {

	private final IDaoUser daoUser;
	private static final Logger logger = LogManager.getLogger(UserService.class);
	private static String _METHOD_NAME;

	@Autowired
	public UserService(IDaoUser daoUser) {
		this.daoUser = daoUser;
	}

	public UserDto addUser(UserDto user) throws CatalogException {
		try {
			_METHOD_NAME = "addUser";
			User newUser = UserMapper.createEntityFromDto(user);
			return UserMapper.createDtoFromEntity(daoUser.save(newUser));
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public List<UserDto> getAllUser() throws CatalogException {
		try {
			_METHOD_NAME = "getAllUser";
			return UserMapper.createListDtoFromEntity(daoUser.findAll());
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public UserDto getUserById(long id) throws CatalogException {
		try {
			_METHOD_NAME = "getUserById";
			return UserMapper.createDtoFromEntity(daoUser.findById(id).orElse(null));
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public int deleteUser(long id) throws CatalogException {
		try {
			_METHOD_NAME = "deleteUser";
			UserDto deleteUser = this.getUserById(id);
			daoUser.delete(UserMapper.createEntityFromDto(deleteUser));
			return 1;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

	public UserDto updateUser(long id, UserDto newUser) throws CatalogException{
		try {
			_METHOD_NAME = "updateUser";
			User entity = UserMapper.createEntityFromDto(newUser);
			User oldUser = daoUser.findById(id).orElse(null);
			if (oldUser != null) {
				User newEntity = UserMapper.createEntityFromDto(newUser);
				newEntity.setId(oldUser.getId());
				return UserMapper.createDtoFromEntity(daoUser.save(newEntity));
			}
			return null;			
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}

}
