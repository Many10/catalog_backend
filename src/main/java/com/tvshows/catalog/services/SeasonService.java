package com.tvshows.catalog.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tvshows.catalog.api.handler.RestHandler;
import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.mapper.SeasonMapper;
import com.tvshows.dto.SeasonDto;

@Service
public class SeasonService {

	private final RestHandler handler;
	private static final Logger logger = LogManager.getLogger(SeasonService.class);
	private static String _METHOD_NAME;
	
	@Autowired
	public SeasonService(RestHandler handler) {
		this.handler = handler;
	}
	
	public List<SeasonDto> getSeasonsByShow(long idShow) throws CatalogException {
		try {
			_METHOD_NAME = "getSeasonsByShow";
			String jsonResponse = handler.executeGetRequest("shows/" + idShow + "/seasons");
			JSONArray json = new JSONArray(jsonResponse);
			List<SeasonDto> seasons = SeasonMapper.createListSeasonFromJsonArray(json);
			return seasons;
		} catch (Exception e) {
			CatalogException cex = new CatalogException(_METHOD_NAME, e);
			logger.error(cex.getMessage());
			throw cex;
		}
	}
	
}
