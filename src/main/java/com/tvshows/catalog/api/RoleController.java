package com.tvshows.catalog.api;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.services.RoleService;
import com.tvshows.dto.RoleDto;

@RequestMapping("api/v1/role")
@RestController
public class RoleController {

	private final RoleService roleService;
	private static final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	public RoleController(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@PostMapping
	public RoleDto addRole(@RequestBody RoleDto roleDto) {
		try {
			logger.info("Añadiendo rol");
			return roleService.addRole(roleDto);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
		
	}
	
	@GetMapping
	public List<RoleDto> getAllRole() {
		try {
			logger.info("Listando roles");
			return roleService.getAllRole();
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}
	
	@GetMapping(path = "{id}")
	public RoleDto getRoleById(@PathVariable("id") long id) {
		try {
			logger.info("Buscando rol por id");
			return roleService.getRoleById(id);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

	@DeleteMapping(path = "{id}")
	public int deleteRoleById(@PathVariable("id") long id) {
		try {
			logger.info("Eliminando rol");
			return roleService.deleteRole(id);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}
	
	@PutMapping(path = "{id}")
	public RoleDto updateRole(@PathVariable("id") long id, @RequestBody RoleDto newRole) {
		try {
			logger.info("Actualizando rol");
			return roleService.updateRole(id, newRole);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}
	
}
