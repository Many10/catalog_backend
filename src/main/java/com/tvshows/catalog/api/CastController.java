package com.tvshows.catalog.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.services.CastService;
import com.tvshows.dto.CastDto;
import com.tvshows.dto.EpisodeDto;

@RequestMapping("api/v1/cast")
@RestController
public class CastController {
	
	private final CastService castService;
	
	@Autowired
	public CastController(CastService castService) {
		this.castService = castService;
	}
	
	@GetMapping(path = "show/{idShow}")
	public List<CastDto> getCastByShow(@PathVariable("idShow") long idShow) {
		try {
			return castService.getCastByShow(idShow);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de shows");
		}
	}

}
