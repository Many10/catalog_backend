package com.tvshows.catalog.api;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.exceptions.CatalogException;
import com.tvshows.catalog.services.UserService;
import com.tvshows.dto.UserDto;

@RequestMapping("api/v1/user")
@RestController
public class UserController {

	private final UserService userService;
	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping
	public UserDto addUser(@RequestBody UserDto userDto) {
		try {
			logger.info("Añadiendo usuario");
			return userService.addUser(userDto);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de añadir usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

	@GetMapping
	public List<UserDto> getAllUser() {
		try {
			logger.info("Listando usuarios");
			return userService.getAllUser();
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de listar usuarios");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

	@GetMapping(path = "{id}")
	public UserDto getUserById(@PathVariable("id") long id) {
		try {
			logger.info("Buscando usuario");
			return userService.getUserById(id);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de buscar usuario");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

	@DeleteMapping(path = "{id}")
	public int deleteUserById(@PathVariable("id") long id) {
		try {
			logger.info("Eliminando usuario");
			return userService.deleteUser(id);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de borrar usuario");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

	@PutMapping(path = "{id}")
	public UserDto updateUser(@PathVariable("id") long id, @RequestBody UserDto newUser) {
		try {
			logger.info("Actualizando usuario");
			return userService.updateUser(id, newUser);
		} catch (CatalogException cex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error en proceso de actualizar usuario");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Se ha presentado un problema");
		}
	}

}
