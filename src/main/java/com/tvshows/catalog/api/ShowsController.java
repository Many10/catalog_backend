package com.tvshows.catalog.api;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.services.ShowService;
import com.tvshows.dto.ShowDto;

@RequestMapping("api/v1/show")
@RestController
public class ShowsController {
	
	private final ShowService showService;
	
	@Autowired
	public ShowsController(ShowService showService) {
		this.showService = showService;
	}
	
	@GetMapping
	public List<ShowDto> getAll() {
		try {
			return showService.getAllShow();
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de listar shows");
		}
	}
	
	@GetMapping(path = "{id}")
	public ShowDto getShowById(@PathVariable("id") long id) {
		try {
			return showService.getShowById(id);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de shows");
		}
	}

	@GetMapping(path = "/filter")
	public List<ShowDto> getShowByFilters(@RequestParam(value = "keyname") String keyname, @RequestParam(value ="language") String language,
			@RequestParam(value = "genre") String genre, @RequestParam(value ="channel") String channel, @RequestParam(value = "scheduleTime") String scheduleTime) {
		try {
			return showService.filterShow(keyname, language, genre, channel, scheduleTime);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de shows");
		}
	}
	
}
