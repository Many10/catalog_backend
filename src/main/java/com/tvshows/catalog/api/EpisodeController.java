package com.tvshows.catalog.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.services.EpisodeService;
import com.tvshows.dto.EpisodeDto;
import com.tvshows.dto.SeasonDto;

@RequestMapping("api/v1/episode")
@RestController
public class EpisodeController {

	private final EpisodeService episodeService;
	
	@Autowired
	public EpisodeController(EpisodeService episodeService) {
		this.episodeService = episodeService;
	}
	
	@GetMapping(path = "season/{idSeason}")
	public List<EpisodeDto> getEpisodeBySeason(@PathVariable("idSeason") long idSeason) {
		try {
			return episodeService.getEpisodesBySeason(idSeason);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de episodios");
		}
	}
	
	@GetMapping(path = "filter/{idShow}/{date}")
	public List<EpisodeDto> getEpisodeBySeason(@PathVariable("idShow") long idShow, @PathVariable("date") String date) {
		try {
			List<EpisodeDto> episodes = episodeService.getEpisodesByDate(idShow, date); 
			if (episodes.isEmpty()) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, 
						"No existen episodios para las fechas seleccionadas");
			}
			return episodes;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de episodios");
		}
	}
	
}
