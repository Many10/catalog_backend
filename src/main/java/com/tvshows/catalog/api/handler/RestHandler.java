package com.tvshows.catalog.api.handler;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.tvshows.catalog.util.Constants;

@Component
public class RestHandler {
	
	public String executeGetRequest(String resource) {
		RestTemplate restTemplate = new RestTemplate();
		String resourceUrl = Constants._API_TV_MAZE + resource ;
		ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
		return response.getBody();
	}

}
