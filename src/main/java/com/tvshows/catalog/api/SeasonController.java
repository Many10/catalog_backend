package com.tvshows.catalog.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.tvshows.catalog.services.SeasonService;
import com.tvshows.dto.SeasonDto;

@RequestMapping("api/v1/season")
@RestController
public class SeasonController {

	private final SeasonService seasonService;
	
	@Autowired
	public SeasonController(SeasonService seasonService) {
		this.seasonService = seasonService;
	}
	
	@GetMapping(path = "show/{idShow}")
	public List<SeasonDto> getSeasonsByShow(@PathVariable("idShow") long idShow) {
		try {
			return seasonService.getSeasonsByShow(idShow);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error en proceso de búsqueda de shows");
		}
	}
}
