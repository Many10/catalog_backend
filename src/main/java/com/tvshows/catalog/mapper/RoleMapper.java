package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import com.tvshows.catalog.entity.Role;
import com.tvshows.dto.RoleDto;

public class RoleMapper {

	public static List<RoleDto> createListDtoFromEntity(List<Role> roles) {
		List<RoleDto> rolesDto = new ArrayList<RoleDto>();
		if (roles != null) {
			for (Role role : roles) {
			    rolesDto.add(createDtoFromEntity(role));
			}
		}
		return rolesDto;
	}
	
	public static RoleDto createDtoFromEntity(Role role) {
		RoleDto roleDto = new RoleDto();
		if (role != null) {
			roleDto.setId(role.getId());
		    roleDto.setRoleName(role.getRoleName());
		    roleDto.setRoleDescription(role.getRoleDescription());
		    roleDto.setPermissions(role.getPermissions());
		}
	    return roleDto;
	}
	
	public static Role createEntityFromDto(RoleDto roleDto) {
		Role role = new Role();
		if (roleDto != null) {
			role.setId(roleDto.getId());
		    role.setRoleName(roleDto.getRoleName());
		    role.setRoleDescription(roleDto.getRoleDescription());
		    role.setPermissions(roleDto.getPermissions());
		}
	    return role;
	}
	
}
