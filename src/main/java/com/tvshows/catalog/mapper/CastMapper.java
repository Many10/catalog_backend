package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tvshows.catalog.util.Util;
import com.tvshows.dto.CastDto;

public class CastMapper {

	public static List<CastDto> createListCastFromJsonArray(JSONArray json) {
		List<CastDto> cast = new ArrayList<CastDto>();
		for (int i=0; i < json.length(); i++) {
		    JSONObject item = json.getJSONObject(i);
		    cast.add(createEpisodeFromJson(item.getJSONObject("person")));
		}
		return cast;
	}
	
	public static CastDto createEpisodeFromJson(JSONObject json) {
	    long id = json.getLong("id");
	    String name = Util.getStringKey(json, "name");
	    String country = json.isNull("country") ? "" : json.getJSONObject("country").getString("name");
	    String birthday = Util.getStringKey(json, "birthday");
	    String gender = Util.getStringKey(json, "gender");
	    String image = json.isNull("image") ? "" : json.getJSONObject("image").getString("medium");
	    String character = json.isNull("character") ? "" : json.getJSONObject("character").getString("name");
	    return new CastDto(id, name, country, birthday, gender, image, character);
	}
}
