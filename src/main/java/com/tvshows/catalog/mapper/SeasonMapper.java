package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tvshows.catalog.util.Util;
import com.tvshows.dto.SeasonDto;

public class SeasonMapper {

	public static List<SeasonDto> createListSeasonFromJsonArray(JSONArray json) {
		List<SeasonDto> seasons = new ArrayList<SeasonDto>();
		for (int i=0; i < json.length(); i++) {
		    JSONObject item = json.getJSONObject(i);
		    seasons.add(createSeasonFromJson(item));
		}
		return seasons;
	}
	
	public static SeasonDto createSeasonFromJson(JSONObject json) {
	    long id = json.getLong("id");
	    int number = Util.getIntKey(json, "number");
	    int cantEpisodes = Util.getIntKey(json, "episodeOrder");
	    String releaseDate = Util.getStringKey(json, "premiereDate");
	    String endDate = Util.getStringKey(json, "endDate");
	    return new SeasonDto(id, number, cantEpisodes, releaseDate, endDate);
	}
	
	
}
