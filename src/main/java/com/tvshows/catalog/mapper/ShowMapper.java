package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tvshows.catalog.util.Util;
import com.tvshows.dto.ShowDto;

public class ShowMapper {

	public static List<ShowDto> createListShowFromJsonArray(JSONArray json, boolean nestedKey) {
		List<ShowDto> shows = new ArrayList<ShowDto>();
		for (int i=0; i < json.length(); i++) {
		    JSONObject item = json.getJSONObject(i);
		    shows.add(nestedKey ? createShowFromJson(item) : createShowFromJson(item.getJSONObject("show")));
		}
		return shows;
	}
	
	public static ShowDto createShowFromJson(JSONObject json) {
	    long id = json.getLong("id");
	    String name = Util.getStringKey(json, "name");
	    String summary = Util.getStringKey(json, "summary");
	    String language = Util.getStringKey(json, "language");
	    String image = json.isNull("image") ? "" : json.getJSONObject("image").getString("medium");
	    String genre = json.isNull("genres") ? "" : json.getJSONArray("genres").toString();
	    String channel = json.isNull("network") ? "" : json.getJSONObject("network").getString("name");
	    String schedule = json.isNull("schedule") ? "" : json.getJSONObject("schedule").getString("time");
	    return new ShowDto(id, name, language, genre, image, channel, schedule, summary);
	}
	
}
