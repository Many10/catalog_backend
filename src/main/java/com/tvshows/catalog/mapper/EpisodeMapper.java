package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tvshows.catalog.util.Util;
import com.tvshows.dto.EpisodeDto;

public class EpisodeMapper {

	public static List<EpisodeDto> createListEpisodesFromJsonArray(JSONArray json) {
		List<EpisodeDto> episodes = new ArrayList<EpisodeDto>();
		for (int i=0; i < json.length(); i++) {
		    JSONObject item = json.getJSONObject(i);
		    episodes.add(createEpisodeFromJson(item));
		}
		return episodes;
	}
	
	public static EpisodeDto createEpisodeFromJson(JSONObject json) {
	    long id = json.getLong("id");
	    String name = Util.getStringKey(json, "name");
	    int order = Util.getIntKey(json, "number");
	    String releaseDate = Util.getStringKey(json, "airdate");
	    String duration = Util.getStringKey(json, "airtime");
	    String summary = Util.getStringKey(json, "summary");
	    String image = json.isNull("image") ? "" : json.getJSONObject("image").getString("medium");
	    return new EpisodeDto(id, name, order, releaseDate, duration, summary, image);
	}
	
}
