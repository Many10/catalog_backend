package com.tvshows.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import com.tvshows.catalog.entity.User;
import com.tvshows.dto.UserDto;

public class UserMapper {
	
	public static List<UserDto> createListDtoFromEntity(List<User> users) {
		List<UserDto> usersDto = new ArrayList<UserDto>();
		if (users != null) {
			for (User user : users) {
			    usersDto.add(createDtoFromEntity(user));
			}
		}
		return usersDto;
	}
	
	public static UserDto createDtoFromEntity(User user) {
	    UserDto userDto = new UserDto();
	    if (userDto != null) {
	    	userDto.setId(user.getId());
	 	    userDto.setUsername(user.getUsername());
	 	    userDto.setPassword(user.getPassword());
	 	    userDto.setFullName(user.getFullName());
	 	    userDto.setRole(RoleMapper.createDtoFromEntity(user.getRole()));
	 	    userDto.setCreatedAt(user.getCreatedAt());
	 	    userDto.setCreatedBy(user.getCreatedBy());
	 	    userDto.setUpdatedAt(user.getUpdatedAt());
	 	    userDto.setUpdatedBy(user.getCreatedBy());
	    }
	    return userDto;
	}
	
	public static User createEntityFromDto(UserDto userDto) {
	    User user = new User();
	    if (userDto != null) {
	    	user.setId(userDto.getId());
	 	    user.setUsername(userDto.getUsername());
	 	    user.setPassword(userDto.getPassword());
	 	    user.setFullName(userDto.getFullName());
	 	    user.setRole(RoleMapper.createEntityFromDto(userDto.getRole()));
	 	    user.setCreatedAt(userDto.getCreatedAt());
	 	    user.setCreatedBy(userDto.getCreatedBy());
	 	    user.setUpdatedAt(userDto.getUpdatedAt());
	 	    user.setUpdatedBy(userDto.getCreatedBy());
	    }
	    return user;
	}
}
