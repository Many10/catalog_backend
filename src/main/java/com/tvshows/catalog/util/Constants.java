package com.tvshows.catalog.util;

public class Constants {

	public static final String _NOT_FOUND_EXCEPTION = "No se han encontrado registros coincidentes para: ";
	public static final String _GLOBAL_EXCEPTION = "Ha ocurrido un error en: ";
	public static final String _API_TV_MAZE = "http://api.tvmaze.com/";
}
