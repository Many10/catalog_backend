package com.tvshows.catalog.exceptions;

import com.tvshows.catalog.util.Constants;

public class CatalogException extends Exception{
	
public CatalogException(String cause, Exception e) {
		
		super(Constants._GLOBAL_EXCEPTION + cause, e);
	}
	
	
}
