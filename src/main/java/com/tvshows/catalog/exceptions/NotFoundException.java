package com.tvshows.catalog.exceptions;

import com.tvshows.catalog.util.Constants;

public class NotFoundException extends Exception {

	public NotFoundException(String cause, Exception e) {
		
		super(Constants._NOT_FOUND_EXCEPTION + cause, e);
	}
	
}
