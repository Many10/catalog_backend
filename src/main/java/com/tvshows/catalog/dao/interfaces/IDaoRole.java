package com.tvshows.catalog.dao.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tvshows.catalog.entity.Role;

@Repository
public interface IDaoRole  extends JpaRepository<Role, Long>  {

}
