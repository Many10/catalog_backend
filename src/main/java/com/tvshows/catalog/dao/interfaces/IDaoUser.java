package com.tvshows.catalog.dao.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tvshows.catalog.entity.User;

@Repository
public interface IDaoUser extends JpaRepository<User, Long> {

}
